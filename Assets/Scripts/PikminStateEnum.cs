public enum PikminState
{
    Idle,
    Follow,
    Carrying,
    Attacking,
    MovingIntoPosition,
    MoveStickFollow,
    InAir
}