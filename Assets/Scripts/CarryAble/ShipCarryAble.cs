public class ShipCarryAble : CarryAble
{
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }

    public override void ReleasePikmin(Pikmin pikmin)
    {
        base.ReleasePikmin(pikmin);
    }

    public override void Initialize()
    {
        base.Initialize();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }
}