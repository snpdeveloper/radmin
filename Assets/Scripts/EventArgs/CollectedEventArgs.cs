﻿using System.Collections.Generic;

public class CollectedEventArgs
{
    public CarryAble ObjectCollected;
    public List<Pikmin> PikminCarrying;
}